cmake_minimum_required(VERSION 3.22)
project(SearchEngine VERSION 1.0)

set(CMAKE_CXX_STANDARD 14)
set(JSON_BuildTests OFF CACHE INTERNAL "")
set(MY_INCLUDE_DIR ${CMAKE_SOURCE_DIR}/include)

include_directories(${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

add_subdirectory(nlohmann_json)
add_subdirectory(src)
add_subdirectory(tests)



target_link_libraries(SearchEngine PRIVATE nlohmann_json::nlohmann_json)
