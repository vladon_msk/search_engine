//#include "InvertedIndex.h"
#include <iostream>
#include <vector>
#include <map>
#include <sstream>
#include <thread>


struct Entry{
    size_t doc_id, count;

    bool operator ==(const Entry& other) const {
        return (doc_id == other.doc_id &&
                count == other.count);
    }
};


class InvertedIndex{
    std::vector<std::string> docs; //список содержимого документа
    std::map<std::string, std::vector<Entry>> freq_dictionary; //частотный словарь
public:
    InvertedIndex() = default;
    /**
    * Обновить или заполнить базу документов, по которой будем совершать
    поиск
    * @param texts_input содержимое документов
    */
    void UpdateDocumentBase(std::vector<std::string> input_docs){
        docs = input_docs;
        for(int i = 0; i < docs.size(); i++){
            std::vector<std::string> words_in_doc(GetWordsFromDoc(docs[i]));
            for(int j = 0; j < words_in_doc.size(); j++){
                freq_dictionary.insert(std::pair<std::string, std::vector<Entry>>(words_in_doc[j], GetWordCount(words_in_doc[j])));
            }
        }

    }
    /**
    * Метод определяет количество вхождений слова word в загруженной базе
    документов
    * @param word слово, частоту вхождений которого необходимо определить
    * @return возвращает подготовленный список с частотой слов
    */
    std::vector<Entry> GetWordCount(const std::string& word){
        std::vector<Entry> entrys;
        for(int i = 0; i < docs.size(); i++){
            std::vector<std::string> words = GetWordsFromDoc(docs[i]);
            int count = 0;
            Entry entry;
            for(int j = 0; j < words.size(); j++){
                if(word == words[j]){
                    count++;
                }
            }
            if(count != 0){
                entry.doc_id = i;
                entry.count = count;
                entrys.push_back(entry);
            }

        }
        return entrys;

    }

    std::vector<std::string> GetWordsFromDoc(std::string& doc){
        std::stringstream string_form_doc;
        string_form_doc.str(doc);
        std::vector<std::string> words;
        while(!string_form_doc.eof()){
            std::string word;
            string_form_doc >> word;
            words.push_back(word);
        }
        return words;
    }

};