#include <iostream>
#include <fstream>
#include <vector>
#include "nlohmann/json.hpp"
using json = nlohmann::json;

class ConverterJSON {
public:
    ConverterJSON() = default;

    //Метод получения содержимого файлов в config.json, возвращает вектор с содержимым файлов
    std::vector<std::string> GetTextDocuments() {
        std::ifstream file("../bin/config.json");
        std::vector<std::string> files;
        json dict;
        file >> dict;
        files = dict["files"];
        std::vector<std::string> text_in_docs;
        for (auto it = 0; it < files.size(); it++) {
            std::ifstream doc(files[it]);
            std::string text;
            if (doc.is_open()) {
                while (!doc.eof()) {
                    std::string buffer;
                    std::getline(doc, buffer);
                    text += buffer;
                }
                text_in_docs.push_back(text);
            } else {
                std::cerr << "Document is empty or does not exit!" << std::endl;
            }
        }
        return text_in_docs;
    };
//Метод считывает поле max_responses для определения предельного количества ответов на один запрос
    int GetResponsesLimit(){
        std::ifstream file("../bin/config.json");
        json dict;
        file >> dict;
        json config = dict["config"];
        return config["max_responses"];
    };

    //Метод получения запросов из файла requests.json возвращает список запросов из файла requests.json
    std::vector<std::string> GetRequests(){
        std::ifstream file("../bin/requests.json");
        std::vector<std::string> requests;
        json dict;
        file >> dict;
        requests = dict["requests"];
        return requests;
    };

    //Положить в файл answers.json результаты поисковых запросов
    void putAnswers(std::vector<std::vector<std::pair<int,float>>> answers){

    };
};