#pragma once
#include <iostream>
#include <vector>
#include <sstream>

struct Entry{
    size_t doc_id, count;

    bool operator ==(const Entry& other) const {
        return (doc_id == other.doc_id &&
                count == other.count);
    }
};

class InvertedIndex{

public:
    std::vector<std::string> GetWordsFromDoc(std::string& doc){
        std::stringstream string_form_doc;
        string_form_doc.str(doc);
        std::vector<std::string> words;
        while(!string_form_doc.eof()){
            std::string word;
            string_form_doc >> word;
            words.push_back(word);
        }
        return words;
    }

};
void setDictionary(std::string& doc){

}
std::vector<LikeEntry> GetWordCount(const std::string& word){
    std::vector<LikeEntry> entrys;
    for(int i = 0; i < docs.size(); i++){
        std::vector<std::string> words = GetWordsFromDoc(docs[i]);
        int count = 0;
        LikeEntry entry;
        for(int j = 0; j < words.size(); j++){
            if(word == words[j]){
                count++;
            }
        }
        if(count != 0){
            entry.doc_id = i;
            entry.count = count;
            entrys.push_back(entry);
        }

    }
    return entrys;
}

